<?php

/**
 * database handler for the great document website
 *
 * @author Marek Sitko
 */
class DatabaseHandler {

    // database parameters
    static $host = 'localhost';
    static $user = 'root';
    static $password = 'rootpass';
    static $dbName = 'books';
    // container for errors
    private $errorMessage;
    // database connection
    private $db;

    /**
     * function to connect to the database
     * 
     * @return boolean TRUE or FALSE
     */
    public function dbConnect() {
        // try to connect to the database
        $this->db = new mysqli(databaseHandler::$host, databaseHandler::$user, databaseHandler::$password);
        // check for errors
        if ($this->db->connect_errno) {
            $this->errorMessage = "Failed to connect to MySQL: " . $this->db->connect_error;
            return FALSE;
        }

        if ($this->dbCheck() !== TRUE) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * function to select database or create it if needed
     * 
     * @return boolean TRUE or FALSE
     */
    private function dbCheck() {

        // try to select our database
        $this->db->select_db(DatabaseHandler::$dbName);
        if (!$this->db->errno) {
            return TRUE;
        }

        // if db dose not exist (arror 1049)
        if ($this->db->errno == 1049) {
            // try to create one
            $query = "CREATE DATABASE " . DatabaseHandler::$dbName;
            $this->db->query($query);
        }
        // in case of an error
        if ($this->db->errno) {
            $this->errorMessage = "Error when creatign DB: " . $this->db->error;
            return FALSE;
        }
        // select created database
        $this->db->select_db(DatabaseHandler::$dbName);
        // create tables
        if ($this->createTables() !== TRUE) {
            return FALSE;
        }
        // import data
        return $this->importData();
    }

    /**
     * function to create tables
     * 
     * @return boolean TRUE or FALSE
     */
    private function createTables() {
        // create books table
        $query = "CREATE TABLE books ("
                . "id       INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                . "title    VARCHAR(50),"
                . "info     TEXT"
                . ")";
        $this->db->query($query);
        // check for errors
        if ($this->db->errno) {
            $this->errorMessage = "Error when creatign TABLE: " . $this->db->error;
            return FALSE;
        }
        // create purchase table
        $query = "CREATE TABLE purchase ("
                . "id       INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                . "book_id  INT,"
                . "email    VARCHAR(50)"
                . ")";
        $this->db->query($query);
        // check for errors
        if ($this->db->errno) {
            $this->errorMessage = "Error when creatign TABLE: " . $this->db->error;
            return FALSE;
        }
        return TRUE;
    }

    /**
     * function to import data into database
     * 
     * @return boolean TRUE or FALSE
     */
    private function importData() {
        if (($handle = fopen("books.csv", "r")) == FALSE) {
            $this->errorMessage = "Cannot open the file books.csv";
            return FALSE;
        }

        while (($data = fgetcsv($handle)) !== FALSE) {

            $num = (count($data) - 3) / 2; // first two fields are column names, the last field is empty so (-3)

            for ($c = 0; $c < $num; $c++) {

                $title = str_replace(array("\r"), "", $data[($c * 2) + 2]);   // extract title 
                $info = $data[($c * 2) + 3];     // extract info
                // insert row 
                $query = $this->db->prepare( "INSERT INTO books VALUES ( NULL, ?, ?)" );
                $query->bind_param( 'ss', $title, $info);
                // execute the query
                $query->execute();
                // check for error
                if ($this->db->errno) {
                    $this->errorMessage = "Error when inserting record: " . $this->db->error;
                    return FALSE;
                }
                $query->close();
            }
        }
        // close the file
        fclose($handle);
        return TRUE;
    }

    /**
     * function to get all books from the database
     * 
     * @return array with records or NULL in case of error
     */
    public function getBooks() {

        // get all records
        $query = "SELECT id, title FROM books";
        $result = $this->db->query($query);
        // check for errors
        if ($this->db->errno) {
            $this->errorMessage = "Error when fetching records: " . $this->db->error;
            return NULL;
        }
        // create array with all records
        $response = array();
        while ($row = $result->fetch_assoc()) {
            $response[] = $row;
        }
        return $response;
    }

    /**
     * function to get a books details from the database
     * @param {integer} id - book id
     * @return book record or NULL in case of error
     */
    public function getBook( $id ) {

        // get all records
        $query = "SELECT * FROM books WHERE id=".$this->db->escape_string($id);
        $result = $this->db->query($query);
        // check for errors
        if ($this->db->errno) {
            $this->errorMessage = "Error when fetching records: " . $this->db->error;
            return NULL;
        }
        return $result->fetch_assoc();
    }

    /**
     * function to save book purchase in the database
     * @param {integer} id - book id
     * @param {string} email - customer's email
     * @return {boolean} TRUE or FALSE
     */
    public function buyBook( $id, $email ) {

        // insert new record
        $query = $this->db->prepare( "INSERT INTO purchase VALUES ( NULL, ?, ?)" );
        $query->bind_param( 'is', $id, $email);
        // execute the query
        $query->execute();
        // check for errors
        if ($this->db->errno) {
            $this->errorMessage = "Error when inserting record: " . $this->db->error;
            return FALSE;
        }
        return TRUE;
    }

    /**
     * function to get the error message
     * 
     * @return String - an error message or NULL if no error occured
     */
    public function getErrorMessage() {
        return $this->errorMessage;
    }

}

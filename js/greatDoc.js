

// create new request
var xhReq = new XMLHttpRequest();

document.onreadystatechange = function() {
    // wait to the document to be completed
    if (document.readyState === "complete") {
        // render our content
        refreshView();
        
    }
};

/**
 * function to make ajax calls
 * 
 * @param {string} url - url to call
 * @param {string} method - GET od POST
 * @param {function()} handler - function to handle events during ajax call
 * @param {Array} parameters - parameters to be sent 
 * @returns {Boolean}
 */
function ajaxCall(url, method, handler, parameters) {

    // check if browser supports XMLHttpRequest
    if (typeof XMLHttpRequest === 'undefined') {
        return false;
    }
    
    // prepare parameters string
    var query = "";
    for (var key in parameters) {
        query += encodeURIComponent(key) + '=' + encodeURIComponent(parameters[key]) + '&';
    }
    query = query.slice( 0, -1 );
    
    // for GET request append parameters at the end of the URL
    if (method === 'GET') {
        if (query.length) {
            url += '?' + query;
        }
        query = null;
    }
    // display loading pic
    document.getElementById('loader').style.display = 'block';
    // open the call with requested method, to given url in asynchronous mode (true)
    xhReq.open(method, url, true);
    // set call event handler
    xhReq.onreadystatechange = handler;
    // set request header for POST method
    if (method === 'POST') {
        xhReq.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    }
    // send the request
    xhReq.send(query);
    return true;
}


/**
 * handler for call to get Books from the API
 */
function getBooksEventHandler() {
    // when response is ready readyStatus will be 4
    if (xhReq.readyState !== 4) {
        return true;
    }
    document.getElementById('loader').style.display = 'none';
    // check the http status code
    switch (xhReq.status) {
        case 200: // OK
            renderBooks(JSON.parse(xhReq.responseText));
            break;
        default:
            // error
            break
    }

}

/**
 * handler for call to get book details
 */
function getBookDetailsEventHandler() {
    // when response is ready readyStatus will be 4
    if (xhReq.readyState !== 4) {
        return;
    }
    document.getElementById('loader').style.display = 'none';
    // check the http status code
    switch (xhReq.status) {
        case 200: // OK
            renderBookDetails(JSON.parse(xhReq.responseText));
            break;
        default:
            // error
            break
    }
}

/**
 * handler for call to get book details
 */
function buyBookEventHandler() {
    // when response is ready readyStatus will be 4
    if (xhReq.readyState !== 4) {
        return;
    }
    document.getElementById('loader').style.display = 'none';
    // check the http status code
    switch (xhReq.status) {
        case 200: // OK
            bookBought();
            break;
        default:
            // error
            break
    }
}

function bookBought() {
    // save bought book in a cookie
    createCookie( 'bought', books.getLatest().title );
    // remove viewed books
    eraseCookie( 'viewed');
    // change page 
    pageChange( 'home');

}

/**
 * function to render books from the API response
 * 
 * @param {type} response
 * @returns {undefined}
 */
function renderBooks(response) {
    // get the last viewed book
    var book = books.getLatest();
    var id = (book?book.id:null);
    // create ordered list of books 
    var html = '';
    for( var key in response.data ) {
        // create list row with book title
        html += '<div class="box-list hoverable" ';
        html += 'onclick="selectBook('+response.data[key].id+', \'' + response.data[key].title + '\')">' + response.data[key].title;
        // if this was the last viewed book add asterix
        if( id == response.data[key].id ) {
            html += '*';
        }
        // closing tag 
        html += '</div>';
            
    }
    html += '</ol>';
    // 
    document.getElementById('content').innerHTML = html;        
}

/**
 * function to render book details from the API response
 * 
 * @param {type} response
 * @returns {undefined}
*/
function renderBookDetails(response) {
    // render title and info
    var html = '<h1>'+response.data.title+'</h1>';
    html += '<h2>Info</h2>';
    html += '<p>'+response.data.info+'</p>';
    // append created html to the container
    document.getElementById('content').innerHTML = html;        
}

/**
 * function to refresh view
 * it reads state of the application from a cookie
 * and render content accordingly
 */
function refreshView() {
    var page = readCookie('page');

    // if cookie 'page' is not set
    if (page === null) {
        page = 'home';
        // create it
        createCookie('page', page);
    }
    // check witch page to render
    switch (page) {
        case 'home':
            renderHome();
            break;
        case 'list':
            renderList();
            break;
        case 'details':
            renderDetails();
            break;
        default:
            break;
    }

}

/**
 * function to create page header
 * @param {string} title
 * @returns {undefined}
 */
function setHeader( title ) {
    document.title = title;
    var html = '<h1>A Great Document Website</h1>';
    html += '<h2>'+title+'</h2>';
    document.getElementById('header').innerHTML = html;
}

/**
 * function to create navigation panel with button
 * @param {type} description - button description
 * @param {type} page - page to change to
 * @returns {undefined}
 */
function setNavPanel( description, page ) {
    // create button
    var html = '<br/><button onClick="pageChange(\''+page+'\')">';
    // add description
    html += description;
    html += '</button>';
    // append to the nav-panel container
    document.getElementById('nav-panel').innerHTML = html;
}

/**
 * function to add input field for email and buy button
 * 
 * @returns {undefined}
 */
function addBuyPanel() {
    var html = '';
    html += '<br/>Please enter your email: <input type="text" onkeydown="hideError();" id="email"/>';
    html += '<button onClick="buyClick()">';
    html += 'Buy';
    html += '</button>';
    html += '<div id="buyError"></div>';
    document.getElementById('nav-panel').innerHTML += html;
}

/**
 * function to hide no email error
 * 
 * @returns {undefined}
 */
function hideError() {
    document.getElementById('buyError').innerHTML = "";
}

/**
 * function to call when Buy button was clicked
 * 
 * @returns {undefined}
 */
function buyClick() {
    var email = document.getElementById('email').value;
    // simple validation if email is not empty
    if( email.length === 0 ) {
        // if it's empty display error and return
        var error = "<p style='color:red'>Please enter your email if you want to buy this item !</p>";
        document.getElementById('buyError').innerHTML = error;
        return;
    }
    var bookId = books.getLatest().id;

    // make ajax call to make a purchase
    ajaxCall('GreatApi.php/buy', 'POST', buyBookEventHandler, { "id" : bookId, "email" : email } );
}

/**
 * function to select book to view
 * 
 * @param {integer} id
 * @param {string} title
 * @returns {undefined}
 */
function selectBook( id, title ) {

    // remember choosen book id in the cookie
    books.addBook(id, title);
    // change page 
    pageChange( 'details');
}

// books object 
var books = {};

/**
 * function to get viewed book sfrom the cookie
 * @returns {Array|Object}
 */
books.getViewed = function() {
    this.viewed = JSON.parse(readCookie('viewed'));
    if( !this.viewed ) {
        this.viewed = {};
    }
    return this.viewed;
};
/**
 * function to save viewed books in the cookie
 * @returns {undefined}
 */
books.saveViewed = function() {
    eraseCookie( 'viewed');
    createCookie( 'viewed', JSON.stringify(this.viewed) );
};
/**
 * function to get all keys sorted from the latest (keys are timestamps)
 * @returns {Array}
 */
books.getSortedKeys = function() {
    if( this.viewed ) {
        this.keys = Object.keys(this.viewed).sort().reverse();
        return this.keys;
    }
};
/**
 * function to add or replace book in the list with new timestamp
 * @param {integer} id - book id
 * @param {string} title - book title
 * @returns {undefined}
 */
books.addBook = function( id, title ) {
    this.getViewed();
    // if book was viewed before remove it
    for( var key in this.viewed ) {
        if( this.viewed[key].id === id ) {
            delete this.viewed[key];
        }
    }
    // remeber viewed book with timestamp
    this.viewed[new Date().getTime()]= { 
        'id'    : id,
        'title' : title        
    };
    this.saveViewed();
};
/**
 * function to get the latest viewed book
 * @returns {Object} - latest book
 */

books.getLatest = function() {
    this.getViewed();
    return this.viewed[this.getSortedKeys()[0]];
};

/**
 * function to change the view
 * @param {type} page - name of the choosen page
 * @returns {undefined}
 */
function pageChange( page ) {
    // save the state of applictaion - current page
    eraseCookie( 'page');
    createCookie( 'page', page );
    // render view
    refreshView();
}

/**
 * function to render the home page
 */
function renderHome() {
    // crete header and nav-panel
    setHeader('Home Page');
    setNavPanel( "View The Document List", 'list');
    
    renderHomeContent();
}

/**
 * function to render content of the home page
 */
function renderHomeContent() {

    var html = "";
    var title = readCookie( 'bought');
    
    // check if book was bought
    if( title ) {
        html += '<p>You have just bought:</p>';
        html += '<p><b>' + title + '</b></p>';
        html += '<p>The full document will be sent to you shortly.</p>';
        eraseCookie( 'bought' );
    }
    else { // if not check if there are any viewed books stored
        books.getViewed();
        // get sorted keys 
        var keys = books.getSortedKeys();
        if( keys.length ) {
            html += '<p>Documents you have viewed (from the most recent):</p>';
            // render each viewed book from teh most recent
            for( var key in keys ) {
                html += '<div class="box-viewed" ';
                html += 'onmouseover="onBookMouseOver('+books.viewed[keys[key]].id+')"';
                html += 'onmouseout="onBookMouseOut()"';
                html += 'onmousemove="onBookMouseMove(event)"';
                html += '>' + books.viewed[keys[key]].title + '</div>';
            }            
        }
        else {
            html += "<p>You haven't viewed any documents yet</p>";            
        }
    }    
    document.getElementById('content').innerHTML = html;
}

var timeout;
var infoBookId;

/**
 * event handler for on mouse over
 * @param {type} event
 * @returns {undefined}
 */
function onBookMouseOver(id) {
    // wait 1s and get book info
    timeout = window.setTimeout( getBookInfo, 1000 );
    infoBookId = id;
}

/**
 * function to get book info
 * @returns {undefined}
 */
function getBookInfo() {
    ajaxCall('GreatApi.php/book', 'GET', getBookInfoEventHandler, { "id" : infoBookId } );
}
/*
 * event handler for mouse out - stop timeout and hides info-box
 * @returns {undefined}
 */
function onBookMouseOut() {
    window.clearTimeout(timeout);
    document.getElementById('info-box').style.display = 'none';
}
/**
 * event handler for mousemove - make sure info-box is never in a cursor way
 * @param {type} e
 * @returns {undefined}
 */
function onBookMouseMove( e ) {
    document.getElementById('info-box').style.top = e.clientY+'px';
    document.getElementById('info-box').style.left = e.clientX+'px';    
}

/**
 * handler for call to get book info
 */
function getBookInfoEventHandler() {
    // when response is ready readyStatus will be 4
    if (xhReq.readyState !== 4) {
        return;
    }
    document.getElementById('loader').style.display = 'none';
    // check the http status code
    switch (xhReq.status) {
        case 200: // OK
            renderBookInfo(JSON.parse(xhReq.responseText));
            break;
        default:
            // error
            break
    }
}

function renderBookInfo( response ) {
    
    document.getElementById('info-box').innerHTML = response.data.info;
    document.getElementById('info-box').style.display = 'block';
}

/**
 * function to render the home page
 */
function renderList() {
    // crete header and nav-panel
    setHeader('Document List');
    setNavPanel( "Return To The Home Page", 'home');
    // make ajax call to get details of requested book
    ajaxCall('GreatApi.php/books', 'GET', getBooksEventHandler, null );
}

/**
 * function to render the home page
 */
function renderDetails() {
    // check the choosen book
    var bookId = books.getLatest().id;
    // crete header and nav-panel
    setHeader('Document Detail');
    setNavPanel( "No Thanks", 'list');
    addBuyPanel();
    // make ajax call to get details of requested book
    ajaxCall('GreatApi.php/book', 'GET', getBookDetailsEventHandler, { "id" : bookId } );
}

/*
 * function to create cookie
 */
function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else
        expires = "";
    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
}

/*
 * function to read cookie
 */
function readCookie(name) {
    var nameEQ = escape(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0)
            return unescape(c.substring(nameEQ.length, c.length));
    }
    return null;
}

/*
 * function to erase cookie
 */
function eraseCookie(name) {
    createCookie(name, "", -1);
}


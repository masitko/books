<?php

include_once('DatabaseHandler.php');

/**
 * Backend API supporting the great document website
 *
 * @author Marek Sitko
 */
class GreatApi {

    // mysqli connection
    private $dbHandler;
    // api response
    private $response;
    // http error code
    private $responseCode;
    // called command with parameters
    private $command;

    /**
     * simple constructor
     */
    public function __construct() {
        // set response to be OK (it will be changed if an error occur)
        $this->response = array(
            'status' => 'ok'
        );
        // set the http response code to OK
        $this->responseCode = 200;

        // get the database handler
        $this->dbHandler = new DatabaseHandler();
    }

    /**
     * function to execute requested API method
     */
    public function execute() {
        // get the whole URI
        $uri = $_SERVER['REQUEST_URI'];

        // split all segments
        $uriArray = explode("/", $uri);
        // and get the last one - it will be our command to execute
        $lastSegment = $uriArray[count($uriArray) - 1];
        // separate command from parameters
        $this->command = explode( '?', $lastSegment );

        // connect to the database - create it if needed
        if ($this->dbHandler->dbConnect() !== TRUE) {
            $this->errorHandler(500, $this->dbHandler->getErrorMessage());
            return FALSE;
        }
        $command = $this->command[0];
        // call method specified as the last URI segment
        $this->$command();

        // return response in json format
        echo json_encode($this->response);
    }

    /**
     * function to get books from the database
     * 
     * on return $this->response contain 'data' array with books or NULL in case of error
     */
    private function books() {
        $this->response['data'] = $this->dbHandler->getBooks();
        if( $this->response['data'] == NULL ) {
            $this->errorHandler(500, $this->dbHandler->getErrorMessage());
        }
    }

    /**
     * function to get book details from the database
     * @param in GET request:
     * id = book id
     * 
     * on return $this->response contain 'data' array with books or NULL in case of error
     */
    private function book() {
        // check if any GET parameters was send with request
        if( !isset( $this->command[1] ) ) {
            $this->errorHandler(500, "Error - please specify book id");
            return;
        }
        // extract all 
        $parameters = explode( '&', $this->command[1] );
        // get the first one - lets assume it is book id
        $id = explode( '=', $parameters[0] );
        // call db handler to get requested book
        $this->response['data'] = $this->dbHandler->getBook($id[1]);
        // in case of error
        if( $this->response['data'] == NULL ) {
            $this->errorHandler(500, $this->dbHandler->getErrorMessage());
        }
    }

    /**
     * function to get book details from the database
     * @param in POST request:
     * id = book id
     * email = customer email
     * 
     * on return $this->status is 'ok' or 'error'
     */
    private function buy() {
        
        // extract POST parameters
        $id = filter_input(INPUT_POST, 'id');
        $email = filter_input(INPUT_POST, 'email');
        // check if id and email were specified
        if( !$id ) {
            $this->errorHandler(500, "Error - please specify book id");
            return;            
        }        
        
        if( !$email ) {
            $this->errorHandler(500, "Error - please specify customer's email");
            return;            
        }
        // buy the book
        if( $this->dbHandler->buyBook($id, $email) !== TRUE ) {
            $this->errorHandler(500, $this->dbHandler->getErrorMessage());
        }
    }

    /**
     * function to handle errors
     * sets error status and error message
     * 
     * @param Int       $errorCode - http code error
     * @param String    $errorMessage - error message
     */
    private function errorHandler($errorCode, $errorMessage) {
        // change status to error
        $this->response['status'] = 'error';
        // set error message
        $this->response['error'] = $errorMessage;
        $this->responseCode = $errorCode;
    }

}

$api = new greatAPI();

$api->execute();


